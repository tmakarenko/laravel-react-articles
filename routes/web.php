<?php



use App\Http\Controllers\TaskController;
use Illuminate\Http\Request;
use App\Task;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $taskController = new TaskController();
    $tasks = $taskController->index();
    return view('show',['tasks' => $tasks]);
});

Route::get('/tasks/create',function(){
    return view('tasks.create');
});

Route::get('/tasks/{id}/update',function($id){
    $task = new Task();
    return view('tasks.update',['task' => $task->find($id)]);
});

Route::post('/tasks',function(Request $request){
    $taskController = new TaskController();
    $tasks = $taskController->store($request);
    return redirect('/');
});

Route::delete('/tasks/{id}',function($id){
    $taskController = new TaskController();
    $taskController->delete($id);
    return redirect('/');
});
