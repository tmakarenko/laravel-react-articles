import React, { Component } from 'react';
import ReactDOM from 'react-dom';


class Main extends Component {
 
  constructor() {
   
    super();
    
    this.state = {
        articles: [],
        currentArticle: null
    }
  }
  
  componentDidMount() {
   
    fetch('/api/articles')
        .then(response => {
            return response.json();
        })
        .then(articles => {
            
            this.setState({ articles });
        });
  }
 
 renderArticles() {
    return this.state.articles.map(article => {
        return (
            
            <li onClick ={()=>this.handleClick(article)} key={article.id} >
                { article.title } 
            </li>      
        );
    })
  }

  handeClick(article){
    this.setState({currentArticle:article});
  }

  render() {
   
    return (
        <div>
            <h3>All articles</h3>
              <ul>
                { this.renderArticles() }
              </ul> 
              <Article article={this.state.currentArticle} />
        </div>
        
        
       
    );
  }
}


ReactDOM.render(
    <Main />,
    document.getElementById('root')
  );