import React, { Component } from 'react';


const Article = ({article}) => {
    
    const divStyle = {
        /*code omitted for brevity */
    }
   
    //if the props product is null, return Product doesn't exist
    if(!article) {
      return(<div style={divStyle}>  Product Doesnt exist </div>);
    }
       
    //Else, display the product data
    return(  
      <div style={divStyle}> 
        <h2> {article.title} </h2>
        <p> {article.description} </p>
        <h3> Status {article.is_active ? 'Available' : 'Out of stock'} </h3>
        
      </div>
    )
  }
   
  export default Article;