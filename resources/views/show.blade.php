@extends('layouts.main')

@section('title')
    Show
@endsection

@section('content')
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <h2>All Tasks</h2>
            <table class="table table-bordered table-stripped">
                <tr>
                    <th>Id</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Is Active</th>
                    <th>Arctions</th>
                </tr>
            @foreach($tasks as $task)
                <tr id={{ $task->id }}>
                    <td>{{ $task->id }}</td>
                    <td>{{ $task->title }}</td>
                    <td>{{ $task->description }}</td>
                    <td>{{ $task->is_active }}</td>
                    <td>
                        {!! Form::open(array('url' => 'tasks/'.$task->id,'class'=>'pull-right')) !!}
                        {!! Form::hidden('_method','DELETE') !!}
                        {!! Form::submit('Delete',array('class'=>'btn btn-warning')) !!}
                        {!! Form::close() !!}
                        {!! Form::open(array('url' => 'tasks/'.$task->id.'/update','class'=>'pull-right')) !!}
                        {!! Form::hidden('_method','GET') !!}
                        {!! Form::submit('Update',array('class'=>'btn btn-info')) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
                
            </table>
        </div>
    </div>
@endsection