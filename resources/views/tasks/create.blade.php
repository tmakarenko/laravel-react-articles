@extends('layouts.main')
@section('title')
    Create
@endsection
@section('content');
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
        {!! Html::ul($errors->all()) !!}
        {!! Form::open(array('url' => '/tasks/','method' => 'put')) !!}
            <div class="form-group">
                {!! Form::label('titile','Title') !!}
                {!! Form::text('title',\Illuminate\Support\Facades\Input::old('title'),array('class'=>'form-control')) !!}
            </div>
            <div class="form-group">
                {!! Form::label('description','Description') !!}
                {!! Form::text('description',\Illuminate\Support\Facades\Input::old('description'),array('class'=>'form-control')) !!}
            </div>
            {!! Form::hidden('is_active',1) !!}
                    
        
                {!! Form::submit('Add Task',array('class'=>'btn btn-primary')) !!}
        {!! FORM::close() !!}
        </div>
    </div>
@endsection