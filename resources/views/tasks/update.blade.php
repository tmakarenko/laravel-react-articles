@extends('layouts.main')

@section('title')
    Update Task
@endsection

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
        {!! Html::ul($errors->all()) !!}
        {!! Form::model(array('url' => '/tasks/{id}')) !!}
            <div class="form-group">
                {!! Form::label('titile','Title') !!}
                {!! Form::text('title',$task->title,array('class'=>'form-control')) !!}
            </div>
            <div class="form-group">
                {!! Form::label('description','Description') !!}
                {!! Form::text('description',$task->description,array('class'=>'form-control')) !!}
            </div>
            {!! Form::hidden('is_active',1) !!}
                    
        
                {!! Form::submit('Add Task',array('class'=>'btn btn-primary')) !!}
        {!! FORM::close() !!}
        </div>
    </div>
@endsection
