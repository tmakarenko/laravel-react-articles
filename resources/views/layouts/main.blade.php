<html>
    <head>
        <title>Laravel Tasks - @yield('title')</title>
        <link href="{{mix('css/app.css')}}" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="header navbar navbar-default">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="navbar-header">
                            <a class="navbar-brand" href="/">Task</a>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                <li class="">
                                    <a href="/tasks/create">Add Task<span class="sr-only">(current)</span></a>
                                </li>
                                
                            </ul>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        <div class="container">
            @yield('content')
        </div>
    </body>
</html>