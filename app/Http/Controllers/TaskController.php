<?php

namespace App\Http\Controllers;

use App\Http\Resources\TaskResource;
use App\Task;
use Illuminate\Http\Request;


class TaskController extends Controller
{
    public function show($id){
        return new TaskResource(Task::find($id));
    }

    public function index(){
        $response = [];
        foreach(Task::all() as $task){
            array_push($response,new TaskResource($task));
        }
        
        return $response;
    }

    public function store(Request $request){
        $task = Task::create($request->all());
        
        return $task;
    }

    public function update(Request $request, $id){
        $task = Task::find($id);
        $task->update($request->all());
        
        return $task;
    }

    public function delete($id){
        $task = Task::findOrFail($id);
        $task->delete();
        
        return null;
    }

}
